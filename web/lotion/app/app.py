import os
from flask import Flask, request, redirect, render_template, render_template_string, send_file, session, Blueprint, url_for
from werkzeug.utils import secure_filename
from werkzeug.exceptions import RequestEntityTooLarge
import secrets
from db_handler import NoteHandler

import uuid
import datetime


app = Flask(__name__)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
BLACKLIST = ['[', ']', '.']

SERVER_HOST = '0.0.0.0'
SERVER_PORT = 8000
UPLOAD_FOLDER = 'uploads'
app.secret_key = 'Q9jM5rCJM0Q2vLDlV7EOI4xBnFGQj+sI'
app.config["MAX_CONTENT_LENGTH"] = 8 * 1024 * 1024
# app.config["DEBUG"] = "development"

def check_allowed_file(file):
    return file.mimetype in ALLOWED_TYPES

def generate_hash(filename):
    return int(os.stat(filename).st_mtime)


def get_all_notes():
    path= os.path.join(APP_ROOT,UPLOAD_FOLDER,session["uid"])
    handler=NoteHandler(path)
    notes=handler.getall()
    return sorted(notes, key=lambda note: note["created_at"], reverse=True)

#creates session


@app.before_first_request
def permanent_session():
    session.permanent = True
    app.permanent_session_lifetime = datetime.timedelta(days=365)


@app.before_request
def register():
    if not session.get("uid"):
        session["uid"] = uuid.uuid4().hex
    path = os.path.join(APP_ROOT, UPLOAD_FOLDER, session["uid"])
    if not os.path.isdir(path):
        os.mkdir(path)

@app.route("/")
def index():
    notes = get_all_notes()
    print(notes)
    if not notes:
        return render_template("index.html", images=None)

    title_error = False
    current = notes[0]
    note = render_template_string(current["note"])
    return render_template("index.html", notes=notes, note=note, title_error=title_error)

@app.route("/upload", methods=["GET","POST"])
def upload_file():
    if request.method == "GET":
        return render_template("upload.html", error=None)

    note = request.form.get("note") or ""
    #checking for template injectiion characters 
    if any( char in note for char in BLACKLIST ):  
        return render_template("upload.html", error="Character not allowed")
    

    target = os.path.join(APP_ROOT, UPLOAD_FOLDER)

    try:
        handler = NoteHandler(os.path.join(APP_ROOT,UPLOAD_FOLDER,session["uid"]))
        handler.add(note)
        return render_template("uploaded.html")
    except:
        return render_template("404.html"), 404

@app.url_defaults
def hashed_url_for_static_file(endpoint, values):
    if endpoint == "static" or endpoint.endswith(".static"):
        filename = values.get("filename")
        if filename:
            if '.' in endpoint:  # has higher priority
                blueprint = endpoint.rsplit('.', 1)[0]
            else:
                blueprint = request.blueprint  # can be None too
            if blueprint:
                static_folder = app.blueprints[blueprint].static_folder
            else:
                static_folder = app.static_folder
            param_name = 'h'
            while param_name in values:
                param_name = '_' + param_name
            values[param_name] = generate_hash(os.path.join(static_folder, filename))

# Error handling

@app.errorhandler(404)
def not_found(e):
    return render_template("404.html"), 404

@app.errorhandler(500)
def server_error(e):
    return render_template("500.html"), 500

@app.errorhandler(413)
@app.errorhandler(RequestEntityTooLarge)
def file_size_limit(e):
    return render_template("413.html"), 413

if __name__ == "__main__":
    app.run(host=SERVER_HOST, port=SERVER_PORT)
