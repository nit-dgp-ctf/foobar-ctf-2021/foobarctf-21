import sqlite3
import os
import time

class NoteHandler:
    def __init__(self, db_dir):
        self.db = os.path.join(db_dir, "db.sqlite3")
        self.column_names = [
            "note",
            "created_at"
        ]

        self.init_db()

    def execute_query(self, query, params=()):
        conn = sqlite3.connect(self.db)
        curs = conn.cursor()
        curs.execute(query, params)
        result = curs.fetchall()
        conn.commit()
        conn.close()

        return result

    def init_db(self):
        if not os.path.exists(self.db):
            self.execute_query(
                """
                CREATE TABLE notes (
                        {} TEXT PRIMARY KEY,
                        {} DATE
                    )
                """.format(
                    *self.column_names
                )
            )

    def exists(self, note):
        result = self.execute_query("SELECT 1 FROM notes WHERE note = ?", (note,))
        return len(result) != 0

    def add(self, note):
        if not self.exists(note):
            t = int(time.time())
            self.execute_query("INSERT INTO notes(note, created_at) VALUES( ?, ?)", (note, t))
            return True
        else:
            return False

    def update(self, note, prevnote):
        if self.exists(prevnote):
            self.execute_query("UPDATE notes SET note = ? WHERE note = ?", (note, prevnote))
            return True
        else:
            return False

    def delete(self, note):
        if self.exists(note):
            self.execute_query("DELETE FROM notes WHERE note = ?", (note,))
            return True
        else:
            return False

    def get(self, note):
        rows = self.execute_query("SELECT note, created_at FROM notes WHERE note = ?", (note,))
        if len(rows) != 0:
            return {
                "note": rows[0][0],
                "created_at": rows[0][1]
            }
        else:
            return None

    def getall(self):
        rows = self.execute_query("SELECT note, created_at FROM notes")
        return [ {
            "note": row[0],
            "created_at": row[1]
        } for row in rows ]
