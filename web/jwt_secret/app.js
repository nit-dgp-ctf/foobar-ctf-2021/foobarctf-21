const express = require('express')
const jwt = require('jsonwebtoken')
const cookieParser = require('cookie-parser')
var fs = require('fs')
const app = express()

const jwt_secret = "badboy"

const flag ="GLUG{us3_5tr0ng_jw7_s3cr37}"

app.set('view-engine', 'ejs')
app.use(express.static(__dirname + '/public'));
app.use(cookieParser())
app.use(express.urlencoded({extended:false}))


app.get('/robots.txt', (req, res) => {
  res.render('pass.ejs')
});


app.get('/', (req, res) => {
  res.render('login.ejs')
});

app.get('/flag', (req, res) => {
  var cookie = req.cookies;
  jwt.verify(cookie['token'],jwt_secret,(err, authData) => {
    if(err) {
      res.sendStatus(403);
    } else {
      var decoded = jwt.decode(cookie['token']);
      if (decoded['user']=='admin'){
   		 res.send(flag);
  	  }else{
    		res.send("ACCESS DENIED YOU ARE NOT THE ADMIN");
  		}
    }
  });
  
});


app.post('/login',(req, res) => {
	var username = req.body.username
  var password = req.body.password
	if(username && password){
	var token = jwt.sign({ 'user': 'error' },jwt_secret, {noTimestamp:true});
    res.cookie('token',token, { maxAge: 900000, httpOnly: true });
    res.redirect(302,'/flag')
	}else{
		res.send("Don't be MAD ?");
	}
});
app.listen(9999, () => console.log('Server started on port 9999'));