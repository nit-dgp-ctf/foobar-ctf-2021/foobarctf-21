<?php
  $url="https://nitdgplug.org/";
  $iurl=$url;
  // echo "Argument: ".$_POST["url"]."\n";
   if( $_SERVER['REQUEST_METHOD'] === 'POST') {
     if(filter_var($_POST["url"] , FILTER_VALIDATE_URL))
     {
       if (strpos($_POST['url'], 'localhost') !== false || strpos($_POST['url'], '127.0.0.1') !== false) {
           echo '<script language="javascript">';
           echo 'alert("Invalid ACCESS")';
           echo '</script>';
           $url=$iurl;
       }
       else $url=$_POST['url'];
     }
    else{
      echo '<script language="javascript">';
      echo 'alert("Invalid URL")';
      echo '</script>';
      $url=$iurl;
    }
   }
   //Initialize cURL.
  //  $ch = curl_init($url);
  //  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
   //Close the cURL handle.
  //  $data=curl_exec($ch);
  //  curl_close($ch);
  //  exec('curl -v -s "'.$url.'"', $data);
   $data = shell_exec('curl -v -s '.$url.'');
  //  $data = file_get_contents($url);
  //  print_r($data);
  //  print $data;
   $dbug = file_put_contents("source.html",$data);
  //  if($dbug) print 1;
  //  else print 0;
?>

<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Allura&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="main.css">
</head>
<body>
<div id="landing">
  <h1 id="heading">MONOCHROMATICITY</h1>
  <div id="boxes">
    <div id="searchbox">
      <div class="wrap">
       <span>Behind all the colorful chaos,</br> lies "monochrome".</br></br></span>
       <form class="search" action = "<?php $_PHP_SELF ?>" method = "POST">
          <input type="text" name="url" class="searchTerm" placeholder="Enter URL of your website">
          <button type="submit" class="searchButton">
            <i class="fa fa-search"></i>
         </button>
       </form>
    </div>
    </div>
    <div id="viewbox">
      <!-- <img src="desktop.png" width="600" height="320" style="position:absolute;z-index:1" alt=""> -->
      <?php
      echo '<iframe id="frame" src="';
      echo $url;
      echo '" frameborder="0" scrolling="yes" target="_parent">';
       ?>
        <p>Your browser does not support iframes.</p>
      </iframe>
      <span class="url"><b>URL: </b>
        <?php
        echo '<a href="';
        echo $url;
        echo '#">';
        echo $url;
        echo '</a>';
         ?>
        (<a href="/source.html" style="text-decoration:none"><b>Source code</b></a>)
      </span>
    </div>
  </div>
</div>
</body>
</html>
