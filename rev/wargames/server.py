import sys
import os
from flag import flag
from time import sleep

menu  = ["HELP GAMES", "LIST GAMES" , "PLAY <game>", "EXIT"]
game = ["FALKEN'S MAZE" , "TIC TAC TOE ", "GLOBAL THERMONUCLEAR WAR"]

def validateLaunchcode(launchcode):

    if (len(launchcode[::-2]) != 12 or len(launchcode[15:]) != 9):
        print("ACCESS DENIED")
        return False



    clen = len(launchcode)
    l1 = launchcode[:8]
    cc = []

    for i in range(0,len(l1),2): 
        q = []                 
        q.append(ord(l1[i]))   
        q.append(ord(l1[i+1]))
        cc.append(q)

    enc = []

    for i in range(len(cc)):
        val1 = cc[i][0] << 1
        val1 ^= 69
        val2 = cc[i][1] << 2
        val2 ^= 10
        enc.append(val1)
        enc.append(val2)

    correct = [159, 218, 153, 214, 45, 206, 153, 374]


    if enc != correct:
        print("ACCESS DENIED")
        return False
    

    l2 =  launchcode[8:16]
    key = "PEACEOUT"

    res = []
    [res.append((ord(key[i]) - ord(l2[i]))) if  i & 1 == 1 else res.append((ord(key[i]) + ord(l2[i]))) for i in range(len(l2))]

    
    ok =  [192, 18, 117, -32, 120, -16, 173, -2]

    if ok != res:
      print("ACCESS DENIED")
      return False


    l3 = launchcode[int(2 * clen / 3):]
    KEY = "There's no way to win"
    I = 7

    KARMA = [123, 47, 86, 28, 74, 50, 32, 114]
    MISSILE = []

    for x in l3:
        MISSILE.append((ord(x) + I ^ ord(KEY[I])) % 255)
        I = (I + 1) % len(KEY)
    
    if KARMA == MISSILE:
        print(flag)
        exit()



def main():
    print("┌───────────────────────────────┐\n")
    print("│  GREETINGS, PROFESSOR FALKEN  │\n")
    print("└───────────────────────────────┘\n")
    welcome = "HOPE YOU ARE FINE !\n"
    for char in welcome:
        sleep(0.1)
        sys.stdout.write(char)
        sys.stdout.flush()
    while True: 
        print("\n")
        print("AVAILABLE COMMANDS:")
        print("\n")
        print(*menu, sep = "\n") 
        choice = input(">").rstrip().lstrip()
        if choice == menu[0]:
            print("'GAMES' REFER TO MODELS , SIMULATIONS AND GAMES \n WHICH HAVE TACTICAL AND STRATEGIC APPLICATION\n\n")
        if choice == menu[1]: 
            print("\n") 
            print(*game, sep = "\n")
            print("\n") 
        if choice == "PLAY":
            print('WHICH GAME ?? CHECK THE LIST\n\n')
        if choice == "PLAY FALKEN'S MAZE":
            print("UNDER MAINTAINCE")
        if choice == "PLAY TIC TAC TOE":
            print("UNDER MAINTAINCE")
        if choice == "PLAY GLOBAL THERMONUCLEAR WAR":
            print("\n") 
            print("AWAITING FIRST STRIKE COMMAND")
            print("------------------------------")
            print("\n") 

            print("PLEASE SPECIFY PRIMARY TARGET")
            print("BY CITY AND/OR COUNTRY NAME")
            target = input(">").rstrip().lstrip()
            print("\n")
            print("PREPARING NUCLEAR STRIKE FOR" + ' ' + target)
            while not validateLaunchcode(input("ENTER LAUNCH CODE:").rstrip().lstrip()):
                pass
        if choice == menu[3]:
            exit()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)


