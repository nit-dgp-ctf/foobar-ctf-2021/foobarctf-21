<h1 align="center">
  <br>
  <a><img src="./logos/foobar.png" width="200"></a>
  <br>  
  FooBar CTF-21
  <br>
</h1>

<p align="center">
   <a href="https://nodejs.org/en/">
    <img src="./logos/node.png" width="60">
  </a>
   &nbsp;&nbsp;
  <a href="https://www.haproxy.com/">
    <img src="./logos/haproxy.png" width="70">
  </a>
   &nbsp;&nbsp;
  <a href="https://nodejs.org/en/">
    <img src="./logos/node.png" width="60">
  </a>
   &nbsp;&nbsp;
  <a href="https://www.python.org/">
    <img src="./logos/python.png" width="40">       
  </a>
   &nbsp;&nbsp;
  <a href="https://cloud.google.com/"><img src="./logos/gcp.png" width="100"></a>
  &nbsp;&nbsp;
  
   <a href="https://www.docker.com/">
    <img src="./logos/docker.png" width="59">       
  </a>
  &nbsp;&nbsp;
   <a href="https://www.docker.com/">
    <img src="./logos/kub.png" width="48">       
  </a>
   &nbsp;&nbsp;
    <a href="https://www.nginx.com/">
    <img src="./logos/nginx.png" width="80">
  </a>
 &nbsp;&nbsp;
   <a href="https://www.cloudflare.com/en-gb/"><img src="./logos/cloudfare.png" width="80"></a>
</p>

<h4 align="center">An aggregation of CTF challenges for FooBar 2021</h4>

<p align="center">
  <a >
    <img src="https://img.shields.io/badge/dependencies-up%20to%20date-brightgreen.svg">
       
  </a>
  <a href="https://opensource.org/licenses/MIT">
    <img src="https://img.shields.io/badge/license-MIT-green.svg">
  </a>
</p>

<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
* [Getting Started](#getting-started)
* [Architecture](#ARCHITECTURE)
* [License](#license)
* [Contributors](#contributors-)


<!-- ABOUT THE PROJECT -->

## About The Project

This is a repository to store CTF challenges to be deployed for FooBar-CTF 2021.

## Getting Started

> The following were the categories of challenges in the CTF:

- Pwn
- Web
- OSINT
- Crypto
- Forensics
- Reversing
- Miscellaneous

All remote challenges have `dockerfile` in there respective folder you can run in your local machine and solve : 

1. Clone the repo
```sh
$ git clone https://gitlab.com/nit-dgp-ctf/foobar-ctf-2021/foobarctf-21.git

```

2.Go to challenge folder

```sh

$ docker build . -t imagename 
$ sudo docker run -dp external-port:container-port imagename
# sudo docker run -dp 1234:9999 imagename 

```


##  Architecture

<p align="centre">  
    <img src="./logos/ctfplatform.png">  
</p>

<p align="centre">  
    <img src="./logos/ctfchallinfra.png">  
</p>




## License

Distributed under the MIT License. See [LICENSE](https://gitlab.com/nit-dgp-ctf/foobar-ctf-2021/foobarctf-21/-/blob/master/LICENSE) for more information.

## Contributors 

Thanks goes to these wonderful people ([emoji key](https://allcontributors.org/docs/en/emoji-key)):

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->

<table>
  <tr>
    <td align="center"><a href="https://github.com/deadlycoder07"><img src="https://avatars.githubusercontent.com/u/56390846?v=4" width="100px;" alt=""/><br /><sub><b>Ramiz mollah</b></sub></a><br /><a href="hhttps://gitlab.com/nit-dgp-ctf/foobar-ctf-2021/foobarctf-21/commits?author=deadlycoder07" title="Documentation">📖</a> <a href="#infra-deadlycoder07" title="Infrastructure (Hosting, Build-Tools, etc)">🚇</a> <a href="https://gitlab.com/nit-dgp-ctf/foobar-ctf-2021/foobarctf-21?author=deadlycoder07" title="Code">💻</a></td>
    <td align="center"><a href="https://github.com/Error-200"><img src="https://avatars.githubusercontent.com/u/57630799?v=4" width="100px;" alt=""/><br /><sub><b>Yash Vardhan</b></sub></a><br /><a href="hhttps://gitlab.com/nit-dgp-ctf/foobar-ctf-2021/foobarctf-21/commits?author=Error-200" title="Documentation">📖</a> <a href="https://gitlab.com/nit-dgp-ctf/foobar-ctf-2021/foobarctf-21?author=Error-200" title="Code">💻</a></td>
    <td align="center"><a href="https://github.com/himanshu272"><img src="https://avatars.githubusercontent.com/u/47220803?v=4" width="100px;" alt=""/><br /><sub><b>Himanshu Shekhar</b></sub></a><br /><a href="https://gitlab.com/nit-dgp-ctf/foobar-ctf-2021/foobarctf-21?author=himanshu272" title="Code">💻</a></td>
    <td align="center"><a href="https://github.com/phantsure"><img src="https://avatars.githubusercontent.com/u/25999504?v=4" width="100px;" alt=""/><br /><sub><b>
    Sampark Sharma</b></sub></a><br /><a href="https://gitlab.com/nit-dgp-ctf/foobar-ctf-2021/foobarctf-21?author=Phantsure" title="Code">💻</a></td>
    <td align="center"><a href="https://github.com/kaki-epithesi"><img src="https://avatars.githubusercontent.com/u/56767725?v=4" width="100px;" alt=""/><br /><sub><b>Abhijit Roy</b></sub></a><br /><a href="https://gitlab.com/nit-dgp-ctf/foobar-ctf-2021/foobarctf-21?author=kaki-epithesi" title="Code">💻</a></td>
    <td align="center"><a href="https://github.com/archi-007"><img src="https://avatars.githubusercontent.com/u/56483460?v=4" width="100px;" alt=""/><br /><sub><b>Archisman Ghosh </b></sub></a><br /><a href="https://gitlab.com/nit-dgp-ctf/foobar-ctf-2021/foobarctf-21?author=archi-007" title="Code">💻</a></td>
      
  </tr>
    
</table>

This project follows the [all-contributors](https://github.com/all-contributors/all-contributors) specification. Contributions of any kind welcome!