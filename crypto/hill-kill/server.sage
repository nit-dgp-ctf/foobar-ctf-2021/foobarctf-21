#!/usr/bin/env sage

from random import randint
from hill import hill_encode
from sage.all import *
import time
import threading
import os

def die(timeout):
    time.sleep(timeout)
    os._exit(0)


def check_det(key,n):
	mat = []
	for i in range(0, len(key), n):
		temp = []
		curr = key[i:i+n]
		for ch in curr:
			temp.append(ord(ch) - ord('a'))
		mat.append(temp)
	R = IntegerModRing(26)
	try:
		M = matrix(R,mat).inverse()
		return 1
	except:
		return 0

def key_gen(n):
	key = ''
	for _ in range(n*n):
		key += chr(randint(1,26) + 96)
	if check_det(key,n) == 0:
		return key_gen(n)
	else:
		return key

def pt_gen():
	pt = ''
	for _ in range(60):
		pt += chr(randint(1,26) + 96)
	return (pt)

def main():
	thr = threading.Thread (target = die, args = (50,))
	thr.start()
	print('HERE YOU GO, YOU HAVE 50 SECONDS TO REACH AT THE TOP')
	for i in range(25):
		n = randint(2,6)
		pt = pt_gen()
		key=key_gen(n)
		ct = hill_encode(key,pt,n)
		ans = input("Enter the decoded Ciphertext:")
		if(ans == pt):
			print("Hurrah your ans is correct")
		else:
			print("KILL SHOT")
			os._exit(0)
	if i == 24:
		print("Congratulations you are at the top of the hill")
		print("Here's your flag : GLUG{17_15_34513r_70_g0_d0wn_4_h1ll_7h4n_up_bu7_7h3_v13w _15_fr0m_7h3_70p}")
main()