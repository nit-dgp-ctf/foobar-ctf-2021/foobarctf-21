from sage.all import *

def hill_encode(key,pt,n):
#     key = "bsdfqwert"
#     pt = "shortexample"
    assert(len(key)==n*n)
    mat = []
    for i in range(0, len(key), n):
        temp = []
        curr = key[i:i+n]
        for ch in curr:
            temp.append(ord(ch) - ord('a'))
        mat.append(temp)

    R = IntegerModRing(26)
    mat = matrix(R,mat)
    #print(mat)
    msg =""

    for i in range(0, len(pt), n):
        num_ct = []
        curr = pt[i:i+n]
        for ch in curr:
            num_ct.append(ord(ch) - ord('a'))
        vector_ct = vector(num_ct)
        #print(vector_ct)
        ct = mat*vector_ct
        #print(ct)
        for j in ct :
            msg += chr(int(j) + ord('a'))
    print(f"KEY        : {key}")
    print(f"CIPHERTEXT : {msg}")
    return msg

def main():
    pass

if __name__ == "__main__": 
    main()
