from binascii import hexlify, unhexlify
from itertools import cycle

def xor(message):
    key = b'\x25\x50\x44\x46'
    message = bytes.fromhex(message)
    return bytes(x ^ y for x, y in zip(message, cycle(key))).hex()

with open('assignment.pdf', 'rb') as f:
    message  = f.read()
    message = hexlify(message).decode().replace('\n', '')
f.close()

with open('assignment', 'wb') as i:
    i.write(unhexlify(xor(message)))
i.close()