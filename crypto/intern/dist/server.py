from Crypto.Util.number import * 
from  flag import flag
from random import getrandbits 

secret = bytes_to_long(flag)

e = 3
p = getPrime(512)
q = getPrime(512)
N = p * q 


menu = """ 
[1].CURR STATE
[2].ENCRYPT FLAG
[3].EXIT

"""

class lcg():
	def __init__(self,m,i,s,n):
		self.m = m  
		self.i = i 
		self.s = s 
		self.n = n 

	def next(self): 
		self.s =  ((self.m * self.s) + self.i) % self.n
		return self.s

	def state(self):
		return self.s



def main():
	m = getrandbits(69)
	s = 69696969
	i = getrandbits(69)
	n = getPrime(69)

	x = lcg(m,i,s,n)
	print("N : {}".format(N))
	print("e : {}".format(e))
    
	while True :
		print(menu)
		choice = input("$ ").rstrip().lstrip()
		if not choice in ["1","2","3"]:
			print("HIGH AF")
			exit()

		if choice == "1":
			print("state for you: {}".format(x.next()))
		elif choice == "2":
			X = x.next()
			ct = pow(secret + X, e, N)
			print("ENC(flag+next_num): {}".format(ct))
		elif choice == "3":
			exit()
            

if __name__ == "__main__":
    main()



