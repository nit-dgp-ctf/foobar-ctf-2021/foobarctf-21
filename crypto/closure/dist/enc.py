from Crypto.Util.number import *
from gmpy2 import gcd
from flag import secret

flag =bytes_to_long(secret)

def keygen():
	p = getPrime(1024)
	i = 1
	while True:
		q = p + i
		if isPrime(q):
			break
		i += 1
	n = p*q
	g = getRandomRange(0,pow(n,2))
	return (g, n)



def encrypt(m, pub, g):
	r = getRandomRange(0,pub)
	assert gcd(r,pub) == 1
	c = pow(g,int(m),pow(pub,2))*pow(r,pub,pow(pub,2)) % pow(pub,2)
	return c


pubkey = keygen()
c = encrypt(flag, pubkey[1], pubkey[0])
print(('g = ' + str(pubkey[0]) + '\n'))
print(('n = ' + str(pubkey[1]) + '\n'))
print(('c = ' + str(c) + '\n'))


