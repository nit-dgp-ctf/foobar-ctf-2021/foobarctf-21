#include <stdio.h>
#include <unistd.h>
#include <string.h>

void vuln(){
	char wish[40];
	printf("So what do you think ?\n");
	gets(wish);
}
int main(){
	setbuf(stdout,0);
	setbuf(stderr,0);
	setbuf(stdin, 0);

	printf("If you want to grant your own wish then you should clear your own path to it\n");
	vuln();
}