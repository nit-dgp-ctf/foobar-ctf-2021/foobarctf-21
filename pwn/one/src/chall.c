#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void get_it(int size)
{
   char buf[512];
   buf[0] = '\0';
   printf("Ok, anything else?\n");
   read(0, buf, size);
   printf("cool \"%s\", here you go.\n", buf);
}

void take_it()
{
   char num[10];
   int size;
   printf("What do you think?\n");
   read(0, num, 9);
   num[9] = '\0';

   size = atoi(num);

   if (size <= 0)
   {
	  printf("That's all huh!!.\n");
	  return;
   }
   if (size > 513)
   {
	  printf("Ok fine.\n");
	  return;
   }

   get_it(size);
}


int main(int argc, char** argv)
{
   setvbuf(stdout, NULL, _IONBF, 0);

   printf("Love is simply an electrical bug in the human neural circuit.\n");
   take_it();
   printf("You are boring dude....\n");
}
