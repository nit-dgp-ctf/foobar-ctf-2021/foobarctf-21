# steghide is used to hide a file in all
# then lsb in one image with password: "this"
# jeremmy hawkeye image is answer

import os
import time
import hashlib

for i in range(1, 11):
    name = hashlib.md5(str(time.time()).encode()).hexdigest()
    print(name + ".jpeg")
    os.system("mv files/original" + str(i) + " files/" + name)
